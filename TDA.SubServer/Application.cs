﻿using ExitGames.Logging;
using ExitGames.Logging.Log4Net;
using log4net.Config;
using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TDA.Common.Enums;
using TDA.Server;

namespace TDA.SubServer {
    class Application : ApplicationBase {

        public ServerPeer_Outbound masterServer { get; private set; }

        private Dictionary<int, PlayerPeer> playerPeers = new Dictionary<int, PlayerPeer>();

        public static Application get() {
            return Application.Instance as Application;
        }

        public void removePlayerPeer(int connectionId) {
            lock(this.playerPeers) {
                if(this.playerPeers.ContainsKey(connectionId)) {
                    this.playerPeers.Remove(connectionId);
                }
            }
        }

        public ServerPeer_Outbound getMasterServer() {
            return this.masterServer;
        }

        protected override PeerBase CreatePeer(InitRequest initRequest) {
            //we only excpet players to connect to a sub server via port 5056
            switch(initRequest.LocalPort) {
                case 5056:
                    PlayerPeer pp = new PlayerPeer(initRequest);
                    lock(this.playerPeers) {
                        this.playerPeers.Add(pp.ConnectionId, pp);
                    }
                    return pp;
                default:
                    throw new Exception("Init request on un-used port: " + initRequest.LocalPort);
            }
        }

        protected override void Setup() {
            log4net.GlobalContext.Properties["Photon:ApplicationLogPath"] = Path.Combine(this.ApplicationRootPath, "log");

            // log4net
            string path = Path.Combine(this.BinaryPath, "log4net.config");
            var file = new FileInfo(path);
            if(file.Exists) {
                LogManager.SetLoggerFactory(Log4NetLoggerFactory.Instance);
                XmlConfigurator.ConfigureAndWatch(file);
            }

            //connect to the master server
            this.masterServer = new ServerPeer_Outbound(this);
            this.masterServer.ConnectTcp(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 4520), "TDA.MasterServer");
        }

        protected override void TearDown() {

        }

        protected override void OnServerConnectionFailed(int errorCode, string errorMessage, object state) {
            Logger.log.FatalFormat("Could not connect to master server: ({0}) - {1}", errorCode, errorMessage);
        }
    }
}
