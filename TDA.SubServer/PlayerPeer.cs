﻿using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhotonHostRuntimeInterfaces;
using TDA.Server;
using TDA.Common.Enums;
using TDA.Server.Helpers;

namespace TDA.SubServer {
    class PlayerPeer : TDA.Server.PlayerPeer {

        public PlayerPeer(InitRequest request) : base(request) {

        }

        protected override void HandleOperationRequest(OperationCodes operationCode, Parameters parameters, Channels channel = Channels.general, bool unreliable = false, bool encrypted = false, bool flush = false) {
            
        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail) {

        }
    }
}
