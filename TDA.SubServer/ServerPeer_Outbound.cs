﻿using Photon.SocketServer.ServerToServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Photon.SocketServer;
using PhotonHostRuntimeInterfaces;
using System.Net;
using System.Management;
using TDA.Common.Enums;
using TDA.Common;
using TDA.Server.Helpers;

namespace TDA.SubServer {
    class ServerPeer_Outbound : TDA.Server.ServerPeer_Outbound {

        public ServerPeer_Outbound(Application application) : base(application) {

        }

        protected void updateServerLoad() {
            if(this.Connected) {
                
                //get the processor time via WMI
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("select PercentProcessorTime from Win32_PerfFormattedData_PerfOS_Processor WHERE Name = '_Total'");
                int processorTime = 0;
                foreach(ManagementObject obj in searcher.Get()) {
                    processorTime = Convert.ToInt32(obj["PercentProcessorTime"]);
                }

                ServerLoad load;
                //work out the load as a ServerLoad
                if(
                    processorTime >= 0 &&
                    processorTime <= 22
                ) {
                    load = ServerLoad.very_low;
                } else if(
                    processorTime > 22 &&
                    processorTime <= 44
                ) {
                    load = ServerLoad.low;
                } else if(
                    processorTime > 44 &&
                    processorTime <= 66
                ) {
                    load = ServerLoad.moderate;
                } else if(
                    processorTime > 66 &&
                    processorTime <= 88
                ) {
                    load = ServerLoad.high;
                } else {
                    load = ServerLoad.very_high;
                }

                this.SendEvent(EventCodes.serverLoadUpdate, new Server.Helpers.Parameters() {
                    { ParameterKeys.serverLoad, load }
                });

                this.RequestFiber.Schedule(this.updateServerLoad, CommonSettings.serverLoadUpdateInterval);           
            }
        }

        protected override void OnConnectionEstablished(object responseObject) {
            //start sending updates
            this.updateServerLoad();
        }

        protected override void OnConnectionFailed(int errorCode, string errorMessage) {
            
        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail) {
            
        }

        protected override void HandleEvent(EventCodes eventCode, Parameters parameters, Channels channel = Channels.general, bool unreliable = false, bool encrypted = false, bool flush = false) {
            
        }

        protected override void HandleOperationRequest(OperationCodes operationCode, Parameters parameters, Channels channel = Channels.general, bool unreliable = false, bool encrypted = false, bool flush = false) {
            
        }

        protected override void HandleOperationResponse(OperationCodes operationCode, Parameters parameters, ResponseCodes responseCode, string errorMessage, Channels channel = Channels.general, bool unreliable = false, bool encrypted = false, bool flush = false) {
            
        }
    }
}
