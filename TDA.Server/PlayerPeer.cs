﻿using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDA.Common.Enums;
using TDA.Server.Helpers;

namespace TDA.Server {
    public abstract class PlayerPeer : ClientPeer {
        public PlayerPeer(InitRequest request) : base(request) { }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters) {
            try {
                this.HandleOperationRequest((OperationCodes)operationRequest.OperationCode, Parameters.FromByteArray(operationRequest.Parameters), (Channels)sendParameters.ChannelId, sendParameters.Unreliable, sendParameters.Encrypted, sendParameters.Flush);
            } catch(Exception ex) {
                this.SendOperationResponseError((OperationCodes)operationRequest.OperationCode, ex.Message, (Channels)sendParameters.ChannelId, sendParameters.Unreliable, sendParameters.Encrypted, sendParameters.Flush);
            }
        }

        protected abstract void HandleOperationRequest(
            OperationCodes operationCode,
            Parameters parameters,
            Channels channel = Channels.general,
            bool unreliable = false,
            bool encrypted = false,
            bool flush = false
        );

        protected SendResult SendEvent(
            EventCodes eventCode,
            Parameters parameters,
            Channels channel = Channels.general, 
            bool unreliable = false, 
            bool encrypted = false, 
            bool flush = false
        ) {
            return this.SendEvent(new EventData() {
                Code = (byte)eventCode,
                Parameters = parameters.ToByteArray()
            }, new SendParameters() {
                Encrypted = encrypted,
                Flush = flush,
                ChannelId = (byte)channel,
                Unreliable = unreliable
            });
        }

        protected SendResult SendOperationResponse(
            OperationCodes opertationCode,
            Parameters parameters,
            Channels channel = Channels.general,
            bool unreliable = false,
            bool encrypted = false,
            bool flush = false
        ) {
            return this.SendOperationResponse(new OperationResponse() {
                OperationCode = (byte)opertationCode,
                Parameters = parameters.ToByteArray()
            }, new SendParameters() {
                ChannelId = (byte)channel,
                Unreliable = unreliable,
                Flush = flush,
                Encrypted = encrypted
            });
        }

        protected void SendOperationResponseError(
            OperationCodes operationCode,
            string errorMessage, 
            Channels channel = Channels.general,
            bool unreliable = false,
            bool encrypted = false,
            bool flush = false
        ) {
            this.SendOperationResponse(new OperationResponse() {
                ReturnCode = (short)ResponseCodes.GENERAL_ERROR,
                OperationCode = (byte)operationCode
            }, new SendParameters() {
                ChannelId = (byte)channel,
                Unreliable = unreliable,
                Flush = flush,
                Encrypted = encrypted
            });
        }
    }
}
