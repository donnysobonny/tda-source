﻿using ExitGames.Logging;
namespace TDA.Server {
    public static class Logger {
        public static readonly ILogger log = LogManager.GetCurrentClassLogger();
    }
}
