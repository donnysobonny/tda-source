﻿using System.Collections.Generic;
using TDA.Common.Enums;

namespace TDA.Server.Helpers {
    public class Parameters : Dictionary<ParameterKeys, object> {

        public static Parameters FromByteArray(Dictionary<byte, object> array) {
            Parameters p = new Parameters();
            foreach(KeyValuePair<byte, object> pair in array) {
                p.Add((ParameterKeys)pair.Key, pair.Value);
            }
            return p;
        }

        public Dictionary<byte, object> ToByteArray() {
            Dictionary<byte, object> array = new Dictionary<byte, object>();
            foreach(KeyValuePair<ParameterKeys, object> pair in this) {
                array.Add((byte)pair.Key, pair.Value);
            }
            return array;
        }

        public bool Exists(ParameterKeys parameter) {
            return this.ContainsKey(parameter);
        }

        public T Get<T>(ParameterKeys parameter) {
            return (T)this[parameter];
        }
    }
}
