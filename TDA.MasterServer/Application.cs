﻿using ExitGames.Logging;
using ExitGames.Logging.Log4Net;
using log4net.Config;
using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDA.MasterServer {
    class Application : ApplicationBase {

        private Dictionary<int, PlayerPeer> playerPeers = new Dictionary<int, PlayerPeer>();
        private Dictionary<int, ServerPeer_Inbound> serverPeers = new Dictionary<int, ServerPeer_Inbound>();

        public static Application get() {
            return Application.Instance as Application;
        }

        public void removePlayerPeer(int connectionId) {
            lock(this.playerPeers) {
                if(playerPeers.ContainsKey(connectionId)) {
                    this.playerPeers.Remove(connectionId);
                }
            }
        }

        public void removeServerPeer(int connectionId) {
            lock(this.serverPeers) {
                if(this.serverPeers.ContainsKey(connectionId)) {
                    this.serverPeers.Remove(connectionId);
                }
            }
        }

        protected override PeerBase CreatePeer(InitRequest initRequest) {
            //player peers use the 5055 udp port, while server peers use the 4520 tcp port
            switch(initRequest.LocalPort) {
                case 5055:
                    PlayerPeer pp = new PlayerPeer(initRequest);
                    lock (this.playerPeers) {
                        this.playerPeers.Add(pp.ConnectionId, pp);
                    }
                    return pp;
                case 4520:
                    ServerPeer_Inbound sp = new ServerPeer_Inbound(initRequest);
                    lock(this.serverPeers) {
                        this.serverPeers.Add(sp.ConnectionId, sp);
                    }
                    return sp;
                default:
                    throw new Exception("Peer tried connected in unknown port: " + initRequest.LocalPort);
            }
        }

        protected override void Setup() {
            log4net.GlobalContext.Properties["Photon:ApplicationLogPath"] = Path.Combine(this.ApplicationRootPath, "log");

            // log4net
            string path = Path.Combine(this.BinaryPath, "log4net.config");
            var file = new FileInfo(path);
            if(file.Exists) {
                LogManager.SetLoggerFactory(Log4NetLoggerFactory.Instance);
                XmlConfigurator.ConfigureAndWatch(file);
            }
        }

        protected override void TearDown() {

        }
    }
}
