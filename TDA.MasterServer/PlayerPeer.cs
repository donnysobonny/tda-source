﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhotonHostRuntimeInterfaces;
using Photon.SocketServer;
using System.IO;
using TDA.Common.Enums;
using TDA.Server;
using TDA.Server.Helpers;

namespace TDA.MasterServer {
    public class PlayerPeer : TDA.Server.PlayerPeer {
        public PlayerPeer(InitRequest request) :base(request) { }

        protected override void HandleOperationRequest(OperationCodes operationCode, Parameters parameters, Channels channel = Channels.general, bool unreliable = false, bool encrypted = false, bool flush = false) {
            
        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail) {
            Logger.log.DebugFormat("Player got disconnected: ({0}) - {1}", reasonCode, reasonDetail);
            Application.get().removePlayerPeer(this.ConnectionId);
        }
    }
}
