﻿using Photon.SocketServer.ServerToServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Photon.SocketServer;
using PhotonHostRuntimeInterfaces;
using TDA.Common.Enums;
using TDA.Server;
using TDA.Server.Helpers;

namespace TDA.MasterServer {
    class ServerPeer_Inbound : TDA.Server.ServerPeer_Inbound {

        public ServerLoad load = ServerLoad.very_high;

        public ServerPeer_Inbound(InitRequest request) : base(request) {

        }

        protected override void HandleEvent(EventCodes eventCode, Parameters parameters, Channels channel = Channels.general, bool unreliable = false, bool encrypted = false, bool flush = false) {
            switch(eventCode) {
                case EventCodes.serverLoadUpdate:
                    if(parameters.Exists(ParameterKeys.serverLoad)) {
                        this.load = parameters.Get<ServerLoad>(ParameterKeys.serverLoad);
                        Logger.log.DebugFormat("Server {0} is telling us that their load is {1}", this.ConnectionId, this.load);
                    }
                    break;
            }
        }

        protected override void HandleOperationRequest(OperationCodes operationCode, Parameters parameters, Channels channel = Channels.general, bool unreliable = false, bool encrypted = false, bool flush = false) {
            
        }

        protected override void HandleOperationResponse(OperationCodes operationCode, Parameters parameters, ResponseCodes responseCode, string errorMessage, Channels channel = Channels.general, bool unreliable = false, bool encrypted = false, bool flush = false) {
            
        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail) {
            Logger.log.DebugFormat("Server got disconnected: ({0}) - {1}", reasonCode, reasonDetail);
            Application.get().removeServerPeer(this.ConnectionId);
        }
    }
}
