﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDA.Common.Enums {
    public enum ServerLoad {
        very_low = 0,
        low = 1,
        moderate = 2,
        high = 3,
        very_high = 4
    }
}
