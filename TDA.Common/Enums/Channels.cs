﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDA.Common.Enums {
    public enum Channels : byte {
        /// <summary>
        /// this channel is for important system messages
        /// </summary>
        system = 0,
        /// <summary>
        /// high-priority updates, such as real-time and high-priority semi-persistent updates
        /// </summary>
        reliableUpdates = 1,
        /// <summary>
        /// low-priority semi-persistent updates
        /// </summary>
        unreliableUpdates = 2,
        /// <summary>
        /// this channel is used for important messages, usually ones that involve actual gameplay such as dealing damage to a target ship.
        /// </summary>
        important = 3,
        /// <summary>
        /// this channel is used for general messages, such as join/leave events
        /// </summary>
        general = 4,
        /// <summary>
        /// this channel is used to send/recieve notifications and chat messages
        /// </summary>
        notifications = 5
    }
}
