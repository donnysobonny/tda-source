﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDA.Common.Enums {
    public enum ResponseCodes : short {
        OKAY = 0,
        GENERAL_ERROR = 500
    }
}
